from struct_data import Node

# open file
equFile = open("equ.mtd", "r")

myNode=Node(0,"+",0)

myNode.setParentIndex(1)

print(myNode.getIndex(),myNode.getValue(),myNode.getParentIndex())

# read and print content
equation=equFile.readline()
print("read equation:",equation)

# parser char from file
for index, ch in enumerate(equation):
    if ch=='[':
        print("[ => %d"%index)
    elif ch==']':
        print("] => %d"%index)
    else:
        print("var/const => %d"%index)
# close file
equFile.close()
