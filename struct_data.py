class Node:
    def __init__(self, index, value, parentIndex):
        self.index = index
        self.value = value
        self.parentIndex = parentIndex

    def setIndex(self, index):
        self.index = index

    def getIndex(self):
        return self.index

    def setValue(self, value):
        self.value = value

    def getValue(self):
        return self.value

    def setParentIndex(self, parentIndex):
        self.parentIndex = parentIndex

    def getParentIndex(self):
        return self.parentIndex
